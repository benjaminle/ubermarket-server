var db = require('../database/db');
const url = require('url');

const bodyParser = require('body-parser');
var hpp = require('hpp');
const rateLimit = require('express-rate-limit');

const limiter = rateLimit({
	windowMs: 1000, //1 second
	max: 1,
});

function setUpRouter(app, express) {
	app.use(limiter);
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(hpp());

	var jsonParser = bodyParser.json();

	app.get('/getPlannerData', function (req, res) {
		const queryObject = url.parse(req.url, true).query;
		db.getPlannerData(queryObject.vendorid, function (returnValue){
			if (returnValue == false) {
				console.log("not in");
				res.send('not in');
			} else {
				console.log("in");
				res.send(returnValue);
			}
		})
	});

	app.get('/getCalendarData', function (req, res) {
		const queryObject = url.parse(req.url, true).query;
		db.getCalendarData(queryObject.vendorid, function (returnValue){
			if (returnValue == false) {
				console.log("not in");
				res.send('not in');
			} else {
				console.log("in");
				res.send(returnValue);
			}
		})
	});

	app.post('/saveFoodPlans', jsonParser, function (req, res) {
		var menuItems = JSON.stringify(JSON.parse(req.body.menuItems));
		var mealData = JSON.stringify(JSON.parse(req.body.mealData));
		console.log(mealData);
		db.saveFoodPlans(
			req.body.vendorId,
			menuItems,
			req.body.mealNumber,
			mealData
		);
		res.send('true');
	});

	app.post('/saveCalendar', jsonParser, function (req, res) {
		var calendar = JSON.stringify(JSON.parse(req.body.calendar));
		console.log(calendar);
		db.saveCalendar(req.body.vendorId, calendar);
		res.send('true');
	});
}

module.exports = function (app, express) {
	setUpRouter(app, express);
	return module.exports;
};
