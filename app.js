const express = require('express');

const app = express();

var routers = require('./routes/routes.js')(app, express);
console.log(routers.string);

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`listening on port ${port}`));
