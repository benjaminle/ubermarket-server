var mysql = require('mysql');

var con = mysql.createConnection({
	host: 'localhost',
	user: 'benjiqle',
	password: 'password',
});

con.connect(function (err) {
	if (err) throw err;
	console.log('connected');
});

let saveFoodPlans = function (vendorId, menuItems, mealNumber, mealData) {
	var checkIfExistsQuery =
		"SELECT * FROM `user-info`.`foods` WHERE vendorId = '" +
		vendorId +
		"' LIMIT 1;";

	try {
		con.query(checkIfExistsQuery, function (err, result) {
			if (result[0] != undefined) {
				console.log(mealNumber);
				var updateQuery =
					"UPDATE `user-info`.foods SET menuList = '" +
					menuItems +
					"', mealNumber = " +
					mealNumber +
					", mealData = '" +
					mealData +
					"' WHERE vendorId = '" +
					vendorId +
					"';";
				console.log(updateQuery);
				try {
					con.query(updateQuery, function (err) {
						if (!err) {
							console.log('success!');
						}
					});
				} catch (err) {
					console.log(err);
				}
			} else {
				var insertQuery =
					"INSERT INTO `user-info`.`foods` (vendorId, mealNumber, menuList, mealData) VALUES ('" +
					vendorId +
					"', '" +
					mealNumber +
					"', '" +
					menuItems +
					"', '" +
					mealData +
					"');";
				console.log(insertQuery);
				try {
					con.query(insertQuery, function (err) {
						if (err) {
							console.log('success!');
						}
					});
				} catch (err) {
					console.log(err);
				}
			}
		});
	} catch (err) {
		console.log(err);
	}
};

let saveCalendar = function (vendorId, calendar) {
	var checkIfExistsQuery =
		"SELECT * FROM `user-info`.`foods` WHERE vendorId = '" +
		vendorId +
		"' LIMIT 1;";
	
	try {
		con.query(checkIfExistsQuery, function (err, result) {
			if (result[0] != undefined) {
				var updateQuery =
					"UPDATE `user-info`.foods SET calendar = '" +
					calendar +
					"' WHERE vendorId = '" +
					vendorId +
					"';";
				console.log('update query ' + updateQuery);
				try {
					con.query(updateQuery, function (err, result) {
						if (err) {
						}
						console.log('success!');
					});
				} catch (err) {
					console.log(err);
				}
			} else {
				var insertQuery =
					"INSERT INTO `user-info`.`foods` (vendorId, , calendar) VALUES ('" +
					vendorId +
					"', '" +
					calendar +
					"');";
				console.log(insertQuery);
				try {
					con.query(insertQuery, function (err) {
						if (err) {
							console.log('success!');
						}
					});
				} catch (err) {
					console.log(err);
				}
			}
		});
	} catch (err) {
		console.log(err);
	}
};

let getPlannerData = function (vendorId, callback)  {
	let getPlannerQuery = "SELECT `vendorId`, `mealNumber`, `menuList`, `mealData` FROM `user-info`.`foods` WHERE vendorId = '" +
							vendorId +
							"' LIMIT 1;";
	try {
		con.query(getPlannerQuery, function (err, result) {
			if (result[0] == undefined)  {
				console.log("user not in");
				callback(false);
			} else {
				console.log(result[0]);
				let jsonobject = {
					vendorId: result[0].vendorId,
					mealNumber: result[0].mealNumber,
					menuList: JSON.parse(result[0].menuList),
					mealData: JSON.parse(result[0].mealData)
				}
				callback(JSON.stringify(jsonobject));
			}
		})
	} catch (err) {
		console.log(err);
	}
}

let getCalendarData = function (vendorId, callback) {
	let getPlannerQuery = "SELECT `calendar` FROM `user-info`.`foods` WHERE vendorId = '" +
							vendorId +
							"' LIMIT 1;";
	try {
		con.query(getPlannerQuery, function (err, result) {
			if (result[0] == undefined)  {
				console.log("user not in");
				callback(false);
			} else {
				let jsonobject = JSON.parse(result[0].calendar);
				callback(JSON.stringify(jsonobject));
			}
		})
	} catch (err) {
		console.log(err);
	}
}

module.exports.saveFoodPlans = saveFoodPlans;
module.exports.saveCalendar = saveCalendar;
module.exports.getPlannerData = getPlannerData;
module.exports.getCalendarData = getCalendarData;
